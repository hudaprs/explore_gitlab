<!DOCTYPE html>
<html>
<head>
	<title>Form Article</title>
</head>
<link rel="icon" href="{{ asset('img/logo.png') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<body style="background-color: #006494">

<div class="container">
        @section('content')
        @if(session('error'))
<div class="alert alert-error">
    {{ session('error') }}
</div>
@endif

@if(count($errors) > 0)
<div class="alert alert-danger">
    <strong>PERHATIAN</strong><br>
	<ul>
        @foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
	<div class="row">
        <div class="col-6 ">
            <div class="card text-left">
              <div class="card-body">
              <h4 class="card-title"><h1>Article #{{ @$articles->id }}</h1></h4>
                        <div class="p-2">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" class="form-control" value="{{ @$articles->title }}" disabled />
                            </div>
                            <div class="form-group">
                                <label for="title">Body</label>
                                <textarea type="text" name="body" id="body" class="form-control" disabled>{{ @$articles->body }}</textarea>
                            </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
	</div>
</div>
</body>
</html>