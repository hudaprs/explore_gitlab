<!DOCTYPE html>
<html>
<head>
	<title>Our Articles</title>
</head>

<link rel="icon" href="{{ asset('img/logo.png') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<style>
    .my-custom-scrollbar {
    position: relative;
    height: 170px;
    overflow: auto;
    }
    .table-wrapper-scroll-y {
    display: block;
	}
	.stuff{
	background-color: white;
	}
	.hor{
		margin-top: 10px;
	}
</style>

<body>


<div class="container">

@section('content')


<h1 class="text-center" style="color: red;">ARTICLE LIST</h1>
     @if(session('success'))
	<div class="alert alert-success mt-3">
		{{ session('success') }}
	</div>
	@endif

	@if(session('error'))
	<div class="alert alert-error mt-3">
		{{ session('error') }}
	</div>
	@endif

	<div class="container">
        <a href="{{ url('/compose') }}" class="btn btn-primary mt-3">CREATE NEW POST</a>
        <h1> ‍ </h1>
		<div class="row">
			<div class="col"></div>
			<div class="col-12">
                    <table class="table table-dark table-striped rounded ">
                        <thead>
                            <tr>
                            <th>Title</th>
                            <th colspan="3"><center>Action</center></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($articles as $data)
                        <tr>
                            <td>{{ $data->title }}</td>
                            <td align="center">
                                <a href="{{ url('/' . $data->id . '/details') }}" class="btn btn-primary">SEE POST</a>
                            </td>
                            <td align="center">
                                    <a href="{{ url('/' . $data->id . '/edit') }}" class="btn btn-primary">EDIT</a>
                            </td>
                            <td align="center">
                                <form action="{{ url('/' . $data->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">DELETE</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
			</div>
		</div>
	</div>
</body>
</html>