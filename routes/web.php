<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ArticleController@index');
Route::get('/compose','ArticleController@create');
Route::get('/{id}/details','ArticleController@seeDetail');
Route::get('/{id}/edit','ArticleController@edit');
Route::post('/','ArticleController@store');
Route::patch('/{id}','ArticleController@update');
Route::delete('/{id}','ArticleController@destroy');
