<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(){
        $data['articles'] = \App\Article::get();
        return view('crud.listArticle',$data);
    }

    public function seeDetail(Request $request,$id){
        $data['articles'] = \App\Article::find($id);
        return view('crud.viewArticle',$data);
    }

    public function create(){
        return view('crud.createArticle');
    }

    public function store(Request $request){
        $rule = [
            'title'=>'required|string',
            'body'=>'required|string'
        ];
        $this->validate($request,$rule);

        $input = $request->all();
        $status = \App\Article::create($input);

        if($status){
            return redirect("/")->with('success','Successfully created post!');
        }else{
            return redirect("/")->with('error','Failed to create post!');
        }
    }

    public function edit(Request $request,$id){
        $data['articles'] = \App\Article::find($id);
        return view('crud.createArticle',$data);
    }

    public function update(Request $request,$id){
        $rule = [
            'title'=>'required|string',
            'body'=>'required|string'
        ];
        $this->validate($request,$rule);

        $input->$request->all();
        $article = \App\Article::find($id);
        $status = $article->update($input);

        if($status){
            return redirect("/")->with('success','Successfully updated post!');
        }else{
            return redirect("/")->with('error','Failed to update post!');
        }
    }

    public function destroy(Request $request,$id){
        $article = \App\Article::find($id);
        $status = $article->delete();

        if($status){
            return redirect("/")->with('success','Successfully deleted post!');
        }else{
            return redirect("/")->with('error','Failed to delete post!');
        }
    }
}
